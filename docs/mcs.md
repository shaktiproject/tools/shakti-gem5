## Mixed ciritical systems - OVERVIEW ##
* real time systems characterized by two or more levels of criticality.
* it is imperative that high critical flows (HCF) meet their deadlines while low critical flows(LCF) can tolerate some dealys.
* sharing of resources between different flows in NoC can lead to different unpredictable latencies  and subsequently complicate the implementation of MCS in many core architectures. 
* Mixed-critical systems require special attention with respect to functional (access protection) and non-functional (performance) isolation. An additional layer of  protection and guaranteed service on the underlying infrastructure enables the efficient adoption of such architectures in safety-critical domains.
 

## 1. DAS(2017) ##
* authors : Mourad Dridi, St ́ephane Rubini
* objectives : **reduce** worst case communciation latency (maximum latnecy, which creates the tail in packets vs latency curve) of HCF; 
**improve network use rate** (injection rate/ throughput), reduce communication latency for LCF. 
 
1 . DAS - double arbitration and switching router, used two flow control or switching techniques : SAF - store and forward  for HCF, wormhole routing for LCF. 

2 . Results : at injection rate(network use rate) = 0.15 / 15%, SystemC-TLM simulator, 4*4 2D mesh, XY routing
* communication dealy for HCF(size = 2flit) reduced by 80%
* communication delay for LCF(size = 8flit) increased by 18% 
* compared with usual solutions based on wormhole router with multiple VCs. 
     
![DAS_router](./pics/DAS1.png  "DAS_router")

![DAS_HCF](./pics/DAS2.png  "DAS_HCF")

![DAS_LCF](./pics/DAS3.png  "DAS_LCF")

3 . Background: 
* A basic VC router (check section2 for more details ) has: 
* a input port, a output port, a routing logic, VC allocator, a switch allocator, and a crossbar. 
* **VC allocator:** 
 
1.   assigns an output channel to a new packet located in one of the input VC buffers. 
2.   allocation involves arbitration (selection) between all the packets residing in different VC's of same port requesting same output channel/port.
 eg: round robin , priority-based, EDF (earliest deadline first), preemptive based, FIFO
 
 * **Switch allocator:**
 1. it arbiters between input ports requests competing for same output port. so, it decides which input port matches with which output port and generates the required crossbar control siganls for the same. 
 2. switching mode :  determines how a packet is allocated to buffers and channels, and when it will recieve a service. Two example: 
 3.  SAF (store and forward): each switch waits for the full packet to arrive, then sends to the next router. buffer size and communication time depend on packet size. 
 4. Wormhole : the network moves in flits. a packet is divided into flits, (like H B B B B T), header flit stores the routing information (like src, dest, number of flits in packet, VC id). If the header flit gets blocked, the remaining flits may span the router at multiple places and block the resources there. 
 
 
 4 . **Proposed router:** 
 1. N+1 VCs : N dedicated to handle HCF with SAF switching mode, one (N+1 th) dedicated to handle LCF with wormhole swithching mode.
 2. two stages of arbitration: 
* Input arbitration unit : any  virtual  channels  of  the  same input  port  can  ask  to  advance  to  different  or  the  same  output  port while the router can accept just one virtual channel from each input port at each cycle. The main task of input arbitration unit is to choose one virtual channel for each input port. 
* Output arbitration unit : many  VCs of different input ports can ask to advance to the same output port  while  only one  virtual  channel  can  be accepted  by  each  output port. The main task of output arbitration unit is to choose one VC for each output port.
These two units are work in two stages: 
The  first  stage  is  a  round  robin  arbitration  between  the Nfirst  virtual  channels  while  the  second  stage  is  a  priority-based arbitration  between  the  winner  of  the  first  stage  and  the  last  virtual channel.
The first stage has to be fair and gives equal chance between all the high-critical flows while, the second stage provides the flit-level preemption  to  high-critical  flows.  So,  the  winner  of  the  first  stage can preempt at flit-level the last virtual channel used by low-critical flows.

## 2. A Virtual Channel Router for On-chip Networks(2004) ##
* authors : Nikolay Kavaldjiev, Gerard J. M. Smit, Pierre G. Jansen
* objectives: propose an architecture of a VC router

**results**: 
1. simple dynamic arbitration which is deterministic and fair
2. size of proposed router is 49% less, speed is 1.4 times more compared to conventional VC router. 

![VCR](./pics/VCR1.png  "VCR")

**Details:**
1. centralized system - with one GPP node performing central coordination functions. It performs  mapping  of  the  newly  arrived  tasks  on  suitable computation units and inter-task communications to network channels. It  also  tries  to  satisfy  Quality  of  Service  (QoS) requirements,   to   optimize   the   resources   usage   and   to minimizing  the  energy  consumption.  The  CCN  does  not perform  scheduling  of  tasks  and  communications,  but  only mapping and allocation. 

2. uses source routing - route a   packet   takes   in   the   network   is   predetermined   and completely described in the packet’s header. 

3. two types of services : 
* GT  (guaranteed  throughput)–  this  is  the  part  of  the traffic for which the network has to give real-time guarantees (i.e. guaranteed bandwidth, bounded latency).
* BE (best effort) – this is the  part of the traffic for which the  network  guarantees  only  fairness  but  does  not  give  any bandwidth and timing guarantees. 

4. Working of virtual channel wormhole router: 
![VCR](./pics/VCR2.png  "VCR")
* At each input port the virtual channels (VCs), 4 in this case,   are   demultiplexed   and   buffered   in   FIFOs.   Status information  is  kept  for  each  of  them.  After  the  FIFOs  they are  multiplexed  again  on  a  single  channel  which  goes  to  a crossbar. 
* Arbitration Unit controls the operation of router. It is determined on a cycle to cycle basis which VC may advance. 
* VC can be in state : {idle, ready, empty, busy}
* task of AU : 
  conflicts  at  the  inputs  –  at  each  cycle  only  one  VC  can advance   from an input port
  conflicts at the outputs – at each cycle an output port can accept data from one input VC only. 
  
  Proposed design: 
  ![VCR](./pics/VCR3.png  "VCR")
  
  The structure of crossbar point is shown: 
    ![VCR](./pics/VCR4.png  "VCR")

  FPGA implementation results:     
   ![VCR](./pics/VCR5.png  "VCR")

## 3. A Wormhole NoC Protocol for Mixed Criticality Systems(2014) ##
* authors : A. Burns, J. Harbin, L. S. Indrusiak 
* objectives : develop a new protocol WPMC which gives adequate  partitioning  between  criticality  levels,  and  to  use resources  efficiently. Cycle  accurate  simulator  is  used  for scenario-based  verification,  and  the  effectiveness  of  the  protocoland its scheduling model is evaluated via message-set generation.

* idea:  to fully support MCS, both periodic and sporadic  traffic. 

Model architecture: 
![VCR](./pics/WPMC1.jpg  "VCR")


* An excellent detailed understanding of baseline wormhole flow control and associated buffer management scheme is presented in first half of paper. 

system model: 
* A network Γ comprises of n real-time traffic-flows (or just flows for short) Γ={ τ1 , τ2 , ... τn }
* Set ofproperties associated with a flow : τi = ( Pi, Ci, Ti, Di, JDi, JIi, IPs, IPd )
* Pi : denotes the priority of a flow 
* Ci : low load network latency (maximum value) (when no contention exists)
* Ti : The lower bound interval on the time between releases of successive messages. 
* Di : deadline, upper bound restriction on network latency. 
* J : denotes the direct and indirect interferences
* IPs, IPd : source and destination IP/nodes. 

Evaluation : 
1. the performance of the WPMC model in terms of improving schedulability is evaluated through the generation and testing of synthetic flow sets.
2. in every evaluation, a flowset is generated consisting of a fixed number of flows. 
3. Each flow is assigned a randomlygenerated source and destination.
4. Each flow is also randomly assigned a criticality (with 50% probability, aiming to producean equal proportion of HI and LO criticality flows)
5. The results indicate that the WPMC protocol outperformsthe baseline case, permitting more flowsets to be scheduled. Inthe case using a 4x4 NoC, for example 10 flows in a flowsetpermit 30% more flowsets to be scheduled than the baseline case. In the case of the larger NoCs (e.g. the 10x10), a larger proportion of flowsets may be scheduled successfully since the larger NoCs provide more locations for flows to be placed without interfering with each other.

Graphs (schedulable flowsets vs no. of flows in a flowset): 
![VCR](./pics/WPMC2.jpg  "VCR")

## 4. IDAMC: A NoC for MCS(2013) ##
* authors : Sebastian Tobuschat, Philip Axer, Rolf Ernst
* objective : to propose a  many-core platform which provides mechanisms to integrate applications of different criticalities on a  single platform.
* this paper justifies that a NoC can be used for MCS. 

* **Details:**
![VCR](./pics/IDAMC1.jpg  "VCR")
1. two functions - safety critical and non-safety critical 
     eg: anti-lock braking system maps to safety/timing critical  task.
     Imagine a error in release of air bag, if the deadline is not met, head of driver can hit the steering which can be hazardous.  (STC) while non-safety/timing critical tasks serve entertainment and comfort events.  
     Above figure shows a multi-core ECU (electronic control unit) 
2. Prevention of error from propogating from non-essential tasks to 
critical tasks is essential. 
3. When STCs and N-STC are mapped onto the same core, the core is mixed critical. 
4. The sharing of resources, like the interconnect or memory, leads to a coupling between the functions accessing the same resource. An application accessing an external memory can delay other application from accessing the same links in the network and the memory itself. Thus the coupling leads to a non-functional interference between the functions and so has major timing implications on the overall system behavior. Two solutions : isolation and analysis
* isolation : TDMA  : static time slots are assigned to different applicaitons following (static slot length decided by highest resource demand of a node )

* **Working of IDAMC:**
![VCR](./pics/IDAMC2.jpg  "VCR")
1. IDAMC - integrated dependable architecture for multi-core systems, supports upto 64 nodes (N0 - N63)
2. a node is composed of 4 tiles and one router
3. A tile is an AMBA system based on the Gaisler GRUB and can include up to 16 processors and other peripherals. 
4. theoretical maximum number of cores in the IDAMC can be upto 4096 cores.
5. A NI consists of buffers and packetization modules to connect the NoC, an address translation, monitoring and control modules for the translation and packet output, and interfaces to the local tile, like AMBA master and slave interfaces.
* Figure: ![VCR](./pics/IDAMC3.jpg  "VCR")
6. IDMAC NoC: 
* uses wormhole routing to transfer data packets, 8VCs per physical link
* **Back suction technique**: The approach is to priotitize non-critical traffic as long as critical traffic makes sufficient progress.Threshold modules at every VC monitor the behavior of the streams. If a too low progress is detected (threshold underflow) a BS signal is forwarded towards the upstream router. Receiv-ing a BS signal, the stream belonging to the appropriate VC is prioritized in a router. The threshold determines how early the prioritization propagates upstream through the network. At the sink a rate limiter guarantees a fixed communication rate.This guarantees that the task still meets its deadlines. It also allows limiting the over-reservation (i.e. an early generation of BS signal at the sink) to not unnecessarily block other traffic. 
* ![VCR](./pics/IDAMC4.jpg  "VCR")

---
**Side notes**: 
* There exist several approaches to achieve predictable communication in NoC. 2 categories : rate controlling and TDMA. 
* The **Mango NoC** implements an asynchronous network providing a globally asynchronous locally synchronous system (GALS). The routers consist of two parts, a best effort (BE) and a guaranteed service router, and implement virtual channels. The GS streams are prioritized over BE streams and a fair-sharing arbitration is used between multiple GS streams. The latency of a message is bounded and mainly depends on the number of VCs sharing a particular connection and the selected arbitration policy. 
---
**What can we do?** : 
1. the back suction technique can be merged with DAS paper. This way, the HCF get priority over LCF but to ensure that LCF don't get starved for longer, back suction can be used. 
