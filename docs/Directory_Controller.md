**Directory controller:**

The communications requirements of a shared memory multiprocessor consists of data requests, data responses and coherence permissions. Coherence permission needs to be obtained before a block can read or write to a cache block. Depending on the cache coherence protocol, other nodes may be required to respond to a permission request.

Directory protocols rely on point-to-point messages rather than broadcasts; this reduction in coherence messages allows this class of protocols to provide greater scalability. Rather than broadcast to all cores, the directory contains information about which cores have the cache block.

Generally, cache coherent shared memory chip multiprocessors requires two message sizes: coherent requests and responses without data ; data messages.

With a directory protocol,each address statically maps to a home node. This home node is responsible for ordering requests for all the addresses which maps to this home node.

![shared_l2_miss](./shared_l2_miss.jpg  "shared_l2_miss")
**Fig1**
The figure shows a shared L2 cache walk-through example where a miss happens for cache block A. 
 
**Gem5 Interconnection network:**

For just traffic injection, gem5 has a dummy cache coherence protocol: 'Garnet_standalone'. It
is used to operate garnet in a standalone manner, agnostic to ISA. The protocol works in conjunction
with Garnet Synthetic Traffic injector only.

This protocol assumes a 1-level cache hierarchy. The role of the cache is to simply send messages
from the cpu to the appropriate directory (based on the address), in the appropriate virtual
network (based on the message type). It does not track any state. Infact, no CacheMemory is created
unlike other protocols. The directory receives the messages from the caches, but does not send any
back. The goal of this protocol is to enable simulation/testing of just the interconnection
network.

Here, the directory controller is only to act as a destination node in the underlying
interconnection network. It  simply pops its incoming queue upon receiving the message and does not track any states.
http://www.gem5.org/Garnet_standalone : Details about implementation are available here. 

**Memory Controller design:**
For a 2 level cache hierarchy: the cache misses from L2 would be handled by main memory. Here, the
request travels via NoC to memory controller. A memory intensive workload can create a heavy demand
of requests which could make memory controllers hot-spots for network traffic.

Consider two cases for placement of memory controller in NoC:
(1) Co-located with processor:
  * sharing of network injection/ejection port with the cache traffic.
(2) Isolated node:
  * memory controller has access to full amount of injection bandwidth
  * many current designs consider placing memory controller to the perimeter of the chip to allow close access to I/O pads.
  *  memory controller has access to full amount of injection bandwidth
  * many current designs consider placing memory controller to the perimeter of the chip to allow close access to I/O pads.

In gem5, memory controller is between last level cache and DRAM (main memory).

![mem_controller](./mem_controller.jpg  "mem_controller")

**Fig2**
The figure shows two cases of memory controller. 