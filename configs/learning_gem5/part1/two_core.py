from __future__ import print_function

import m5
import sys
from m5.objects import *

m5.util.addToPath('../../')

from caches import *

from common import SimpleOpts

# Set the usage message to display
SimpleOpts.set_usage("usage: %prog [options] <binary to execute>")

# Finalize the arguments and grab the opts so we can pass it on to our objects
(opts, args) = SimpleOpts.parse_args()

# get ISA for the default binary to run. This is mostly for simple testing
isa = str(m5.defines.buildEnv['TARGET_ISA']).lower()

# Default to running 'hello', use the compiled ISA to find the binary
binary = 'tests/test-progs/hello/bin/' + isa + '/linux/a.out'

# Check if there was a binary passed in via the command line and error if
# there are too many arguments
if len(args) == 1:
    binary = args[0]
elif len(args) > 1:
    SimpleOpts.print_help()
    m5.fatal("Expected a binary to execute as positional argument")

print("Version of python is :::::::::::: " + sys.version)

#system config
system = System(cpu = [TimingSimpleCPU(cpu_id=i) for i in xrange(2)])
for i in range(2):
	print([TimingSimpleCPU(cpu_id=i)])

for i in xrange(2):
	print(system.cpu[i])


system.clk_domain = SrcClockDomain()
system.clk_domain.clock = '1GHz'
system.clk_domain.voltage_domain = VoltageDomain()

system.mem_mode = 'timing'
system.mem_ranges = [AddrRange('512MB')]

system.cpu_voltage_domain = VoltageDomain()
system.cpu_clk_domain = SrcClockDomain(clock = '1GHz',voltage_domain= system.cpu_voltage_domain)

system.membus = SystemXBar()
system.l2bus = L2XBar()
#multiprocess =[Process(cmd = 'tests/test-progs/hello/bin/riscv/linux/a.out', pid = 100 + i) for i in xrange(2)]
multiprocess =[Process(cmd = 'tests/test-progs/hello/src/a.out', pid = 100 + i) for i in xrange(2)]

#cpu config
for i in xrange(2):
    system.cpu[i].icache = L1ICache(opts)
    system.cpu[i].dcache = L1DCache(opts)
    system.cpu[i].icache.connectCPU(system.cpu[i])
    system.cpu[i].dcache.connectCPU(system.cpu[i])
    system.cpu[i].icache.connectBus(system.l2bus)
    system.cpu[i].dcache.connectBus(system.l2bus)
    system.cpu[i].createInterruptController()
    system.cpu[i].workload = multiprocess[i]
    system.cpu[i].createThreads()

system.l2cache = L2Cache()
system.l2cache.cpu_side = system.l2bus.master
system.l2cache.mem_side = system.membus.slave
system.system_port = system.membus.slave

system.mem_ctrl = DDR3_1600_8x8()
system.mem_ctrl.range = system.mem_ranges[0]
system.mem_ctrl.port = system.membus.master

root = Root(full_system = False, system = system)

# instantiate all the objects that we have created above

m5.instantiate()

print("The simulation is going to start in 3 2 1 .. GOOOO! ")
exit_event = m5.simulate()
print('Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause()))
