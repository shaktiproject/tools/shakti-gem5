**Aim** : To make changes in switch arbitration to give priority to High critical flows over Low critical flows. 


**Overview :** The VNET0 , VNET1 are marked as high critcal while VNET2 is marked as low critical.
During input port arbitration, first round robin is performed between VCs of VNET0, VNET1 (default 8 VCs) to find winner VC. If no VC from these high critical VNETs participates, then round robin selection is performed in low critical VNET2.
Similarly, during output port arbitration, for each input port, first look for any high critical VNET participating in a round robin manner. If not, look for low critical VNET in round robin manner.

Basically, to give priority to high critical VNETs, I target the order of checking of VCs from the round robin pointer in input and output port arbitration to ensure that first all high critical VNETs are checked

**Changes:**
1. Input Arbitration : 
*  Round Robin in VNET0, VNET1
*  Check all VCs of High critical flow before checking any VC of low critical. 
*  Ensure round robin in Low critical VCs

2. Output Arbitration:
*  Round Robin in output port arbitration. 
*  Check all input ports from the round robin pointer to find first high critical VC. 
*  If no high critical VC is participating, look for all input ports to find first low critical VC.


**Three cases :**
*  baseline with all VNETs of same criticality and round robin arbitration. 
*  2 vnets for HCF, 1 vnet for LCF with vnet depth of 4 flits, round robin in HCF, pointer starts with first VC in LO vnet. 
*  Same as (2) with round robin in LO vnet too. 


**Results:**
For a mesh configuration of 2*2, XY routing, 10,000 cycles, injection rate of 0.2, uniform_random synthetic traffic : 
1. Running the avg_lat.py to obtain latency values for each VC for input arbitration changes.
    * [Baseline](https://pastebin.com/gsyuN8K3)
    * [Input port arbitration without RR in LO](https://pastebin.com/zpqcs6VD) 
    * [Better Input port arbitration](https://pastebin.com/7PUZSjnp)
    * [With Output port arbitration](https://pastebin.com/HXiRHtVj)
