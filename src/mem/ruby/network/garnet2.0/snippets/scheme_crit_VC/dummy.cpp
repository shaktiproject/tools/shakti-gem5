//coding the ordering check
// hardcoding for vc_per_vnet = 4, total vnets = 3

#include<iostream>

int get_vnet(int vc){
    return (vc/4);

    }

int get_index_hi(int invc){

    int hi_order[6] = {0,1,4,5,8,9};
    for(int i =0; i < 6; i++)
        if(hi_order[i]==invc)
            return i;

    }

int get_index_lo(int invc){

    int lo_order[6] = {2,3,6,7,10,11};
    for(int i =0; i < 6; i++)
        if(lo_order[i]==invc)
            return i;

    }

// write the ordering check in form of function

int main() {
    //doing ordering check only for HI vcs = {0,1,4,5,8,9}
    // total vcs are = {0,1,2,3,4,5,6,7,8,9,10,11}
    int lo_invc = 10;
    int invc = 5;
    int vc_per_vnet = 4;
    int m_num_vcs = 3*vc_per_vnet;
    int arr[m_num_vcs];
    //since invc = 1, order should be 1 4 5 8 9 0

    /*
    === This snippet can be used to fill hi_order and lo_order array ===
    // first let's print for the trivial case: when invc is always 0
    // so order always remains : 0 1 4 5 8 9 2 3 6 7 10 11
    for(int i =0; i < 3; i++){
        int vnet = i;
        for(int j = 0; j  < 2; j++){
            arr[vnet*2+j] = vnet*4 + j;
            arr[vnet*2 + j +6] = vnet*4 + 2 + j;
        }
    }

    */
    int hi_order[6] = {0,1,4,5,8,9};
    int lo_order[6] = {2,3,6,7,10,11};
    int index_hi = get_index_hi(invc);
    int index_lo = get_index_lo(lo_invc);

    std::cout << "starting from invc = " << invc << std::endl;
    for(int i =0; i < 6; i++)
        {
            arr[i] = hi_order[(index_hi+i)%6]; // 3 4 5 6 7 8 : 3 4 5 0 1 2
            arr[i+6] = lo_order[(index_lo+i)%6];
            //std::cout << arr[i] << std::endl;

        }
    // order should be : 5 8 9 0 1 4 10 11 2 3 6 7
    //lets check the complete array now:
    for(int i =0; i < m_num_vcs; i++)
        std::cout << arr[i] << std::endl;

}
