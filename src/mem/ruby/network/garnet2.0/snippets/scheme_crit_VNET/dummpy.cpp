#include<iostream>
// tabspace of gedit	here
// tabspace of vimmm  here

/**********
context : input port arbitration
This is a dummy code: for the purpose of input arbitration with criticalities assigned to
vnets, the round robin pointer happens in both low and high critical vnet. Now, before checking
any low critical vent, we need to check all high critical VCs.
Basically, in the input port arbitration, the order of checking decides the way we prioritize
VCs.

***********/
int main() {
  int invc = 5;
  int m_num_vcs = 12;
  int m_vc_per_vnet = 4;
	int old_lo_invc = 9;
	//int invc = m_round_robin_invc[inport];
	// if invc  = 5
	// checkig order should be 5 6 7 0 1 2 3 4 8 9 10 11
	// here the order will be 5 6 7 8 9 10 11 0 1 2 3 4
	//cout << "Input arbitration for: ( " << inport << ", " << invc << " )"<<endl;

	int arr[3*m_vc_per_vnet];

	//int old_lo_invc =  10; // should be defined globally as need to store state
	// for next cycle
	//new correct order should : 11 8 9 10

	int lo_arr[m_vc_per_vnet]; // ex : arr[10]

	int count = 0;
	// i < 12-8-1

	for (int i = 0 ; i < 3*m_vc_per_vnet - old_lo_invc - 1; i++ )
	{
		lo_arr[i] = old_lo_invc + i + 1;
		count++;
	}

	int resume_index = 3*m_vc_per_vnet - old_lo_invc - 1 ;
	int left = m_vc_per_vnet - count;

	for (int i =0; i  < left; i++)
		lo_arr[resume_index + i] = 2*m_vc_per_vnet + i;

  for (int i =0 ; i < 8-invc; i++)
  	arr[i] = invc + i;

  for (int i = 0; i < invc; i++ )
  	arr[8-invc + i] = i;

  for (int i = 0; i < 4 ; i++)
  	arr[8 + i] = lo_arr[i];

  for(int i =0 ; i < 12; i++)
    std::cout << arr[i] << std::endl;

  std::cout << "yo" << std::endl;
  return 0;
  }
