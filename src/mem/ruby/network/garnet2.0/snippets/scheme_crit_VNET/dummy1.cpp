
#include<iostream>
#include <vector>


int get_vnet(int invc)
{
    int m_vc_per_vnet = 4;
    int vnet = invc/m_vc_per_vnet;
    return vnet;
}

int main(){


int inport = 2; // round robin start pointer
int m_num_inports = 4;
int m_num_outports = 4;
std::vector<std::vector<bool>> m_port_requests;
std::vector<std::vector<int>> m_vc_winners;

m_port_requests.resize(m_num_outports);
m_vc_winners.resize(m_num_outports);

for(int i =0; i < m_num_outports; i++)
	{
	m_port_requests[i].resize(m_num_inports);
    m_vc_winners[i].resize(m_num_inports);

	for(int  j = 0; j < m_num_inports; j++)
	{
		m_port_requests[i][j] = false;
		m_vc_winners[i][j] =0;
	}
}

//manual tuning .. setting the values
m_port_requests[0][0] = true;
m_port_requests[0][1] = true;
m_port_requests[0][2] = true;
m_port_requests[0][3] = true;
m_port_requests[1][1] = true;
m_port_requests[1][2] = true;
m_port_requests[2][1] = true;
m_port_requests[3][3] = true;

m_vc_winners[0][0] = 9;
m_vc_winners[0][2] = 4;
m_vc_winners[0][3] = 5;
m_vc_winners[0][1] = 3;
m_vc_winners[1][1] = 10;
m_vc_winners[1][2] = 6;
m_vc_winners[2][1] = 8;
m_vc_winners[3][3] = 6;

/*
columns are input ports
rows are output ports
There are two matrix here : which input port has placed a request, and which VC is particiapting
for the request

// This matrix is for port request
    0     1     2     3
0  (T)   (T)   (T)   (T)

1   F    (T)   (T)    F

2   F    (T)    F     F

3   F     F    (T)    F

// This matrix is for vc_winner, if the port request is false it doens't matter what value it has
    0     1     2     3
0  (9)   (3)   (4)   (5)

1   x    (10)  (6)    x

2   x    (8)    x     x

3   x     x     x    (6)

*/

/*
The output port arbitration order for each output port depends on if the VC is lo/hi and
where does our round robin pointer starts with.
*/

/*
Here, order of checking of input port decides which input port gets priority
We want that all input ports of high cricitcality VC should be checked before selecting any
low criticality VC.
So, If there are any high criticality VC participating : we set the inport to it.. starting checking
from the round robin pointer.

*/

/*
Since round robin pointer starts checking from inport2
Correct order of output should be:
outport 0 : inport 2
outport 1 : inport 2
outport 2 : inport x (as no hi flow is participating, 1 will get selected using the normal O/p Arb
outport 3 : inport 3

*/
int temp = inport;
for(int outport = 0; outport < m_num_outports; outport++)
	for(int i = 0 ; i < m_num_inports; i++)
            {

                int dummy_var = (temp + i) % m_num_inports;
                // for inport = 2 ; order of checking is 2 3 0 1
                if(m_port_requests[outport][dummy_var]){
                    int tempy_vc = m_vc_winners[outport][dummy_var];
                    if(get_vnet(tempy_vc) != 2)
                        {
                        	inport = dummy_var;
                        	std::cout << "this inport is selected: " << inport << " at output port: " << outport << std::endl;
                          break;
                        }
                }

            }



}
