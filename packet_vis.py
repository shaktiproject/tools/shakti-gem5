"""
Use python3
This script generates a packet picture based on input packet size.
Example:
(1) Packet size = 1
Output : - - - - - - -
         | HEAD_TAIL |
         - - - - - - -

(2) Packet size = 2
Output : - - - - - - - -
         | HEAD | TAIL |
         - - - - - - - -

(3) Packet size = 5
Output : - - - - - - - - - - - - - - - - - -
         | HEAD | BODY | BODY | BODY | TAIL |
         - - - - - - - - - - - - - - - - - -
"""

head_tail = """
            - - - - - - -
            | HEAD_TAIL |
            - - - - - - -
            """
head = """- - -
       HEAD |
       - - -"""
body = """ - - -
       BODY |
       - - -"""

tail = """- - -
       TAIL |
       - - -"""



packet_sz = int(input("Please enter packet size"))

if(packet_sz == 1):
  print(head_tail)


if(packet_sz != 1):
  for i in range(4*packet_sz):
    print("- ", end = '')
  print("\n", end='')
  print("| ", end='')
  print(" HEAD |", end='')
  for i in range(packet_sz-2):
    print(" BODY |", end='')

  print(" TAIL |", end='')
  print("\n", end = '')
  for i in range(4*packet_sz):
    print("- ", end = '')

"""

if(packet_sz != 1):
  print(head, end='')

  for i in range(packet_sz - 2):
    print(body,end='')

  print(tail,end='')
"""
print("\n", end='')

