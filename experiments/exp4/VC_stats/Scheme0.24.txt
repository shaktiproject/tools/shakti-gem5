
=======
VC0stats:
avg_lat is: 11.31341487868458
max_lat is:29
total packets: 9974
95% avg_lat: 10.879549987863182
95% latency stopper is: 17
======

=======
VC1stats:
avg_lat is: 11.277893857083159
max_lat is:26
total packets: 2393
95% avg_lat: 10.848747443200564
95% latency stopper is: 17
======

=======
VC2stats:
avg_lat is: 11.54516129032258
max_lat is:24
total packets: 310
95% avg_lat: 11.086587436332767
95% latency stopper is: 18
======

=======
VC3stats:
avg_lat is: 11.518518518518519
max_lat is:22
total packets: 27
95% avg_lat: 10.604288499025342
95% latency stopper is: 17
======

=======
VC4stats:
avg_lat is: 10.60550184391508
max_lat is:25
total packets: 10033
95% avg_lat: 10.23894831267344
95% latency stopper is: 16
======

=======
VC5stats:
avg_lat is: 10.840936616875252
max_lat is:25
total packets: 2477
95% avg_lat: 10.473195503899028
95% latency stopper is: 16
======

=======
VC6stats:
avg_lat is: 10.721875
max_lat is:24
total packets: 320
95% avg_lat: 10.358552631578947
95% latency stopper is: 16
======

=======
VC7stats:
avg_lat is: 11.52
max_lat is:21
total packets: 25
95% avg_lat: 10.610526315789473
95% latency stopper is: 15
======

=======
VC8stats:
avg_lat is: 34.23836442434521
max_lat is:232
total packets: 5307
95% avg_lat: 31.2562355578035
95% latency stopper is: 71
======

=======
VC9stats:
avg_lat is: 35.09278613306143
max_lat is:183
total packets: 3923
95% avg_lat: 32.001287950950534
95% latency stopper is: 72
======

=======
VC10stats:
avg_lat is: 37.567680133277804
max_lat is:299
total packets: 2401
95% avg_lat: 34.14454503605954
95% latency stopper is: 78
======

=======
VC11stats:
avg_lat is: 39.579166666666666
max_lat is:174
total packets: 1200
95% avg_lat: 36.012280701754385
95% latency stopper is: 81
======
