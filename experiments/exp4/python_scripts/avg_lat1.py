average = 0
total_packets = 0
for i in range(12):
    #filename = input("Which VC? ")
    fname = 'lat/' + 'latency' + str(i) + '.txt'
    print(" ")
    with open(fname) as f:
      content = f.readlines()

    content = [x.strip() for x in content]
    lis = []

    for x in content:
      lis.append(int(x))

    total = 0
    for x in lis:
      total += x

    # edit by rishabh
    average += total
    total_packets += len(lis)# end

    lis1 = lis[: ]# copy of original list
    print("=======")
    print("VC" + str(i) + "stats: ")
    print("avg_lat is: " + str(total / float(len(lis))))
    print("min_lat is:" + str(min(lis)))
    print("max_lat is:" + str(max(lis)))
    print("total packets: " + str(len(lis)))

    # finding 95 percentile..where does the 95 percentile packets lie ?
    #step1 : sort the list
    #step2: sweep through the list till 0.95 * len(lis)(covering 95 % packets)
    # step3: find the average latency for them

    lis1.sort()
    total = 0

    for x in range(int(0.95 * len(lis1))):
      total += lis1[x]

    print("95% avg_lat: " + str(total / (float(0.95 * len(lis1)))))

    # value of latency at which to the left lies 95 % of packets
    nintey_five_latency = lis1[int(0.95 * len(lis1))]
    print("95% latency stopper is: " + str(nintey_five_latency))
    print("======")
print("total latency is: " + str(average))
print("total packets are: " + str(total_packets))
print("Average latency of network is: " + str(average / (1.0 * total_packets)))
