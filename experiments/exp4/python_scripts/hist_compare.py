# this script takes input of two files : and outputs two histograms in one
# picture. Can be used to compare VC performance.


#!/usr/bin/env python
# get file1
fname1 = input("> Which file? ")
inj_rate1 = input("> Corresponding injection rate? ")
with open(fname1) as f:
  content1 = f.readlines()

content1 = [x.strip() for x in content1]
lis1 = []
for x in content1:
  lis1.append(int(x))

#get file2
fname2 = input("> Which file? ")
inj_rate2 = input("> Corresponding injection rate? ")
with open(fname2) as f:
  content2 = f.readlines()

content2 = [x.strip() for x in content2]
lis2 = []
for x in content2:
  lis2.append(int(x))


#print(lis)

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


#plotting graph1
minimum = min(lis1)
maximum = max(lis1)
print("Total packets: " + str(len(lis1)))
print("The max latency is : " + str(maximum) + " Cycles")
print("The min latency is : " + str(minimum) + " Cycles")
legend = ['injection rate = ' + inj_rate1]
bins = maximum - minimum
plt.subplot(221) # the first figure
plt.hist(lis1,bins)
plt.xlabel("Latency ")
plt.ylabel("Number of packets")
plt.title("Baseline")
plt.legend(legend)
#plt.show()

#plotting graph2
minimum = min(lis2)
maximum = max(lis2)
print("Total packets: " + str(len(lis2)))
print("The max latency is : " + str(maximum) + " Cycles")
print("The min latency is : " + str(minimum) + " Cycles")
legend = ['injection rate = ' + inj_rate2]
bins = maximum - minimum
plt.subplot(222) # the first figure
plt.hist(lis2,bins)
plt.xlabel("Latency ")
plt.ylabel("Number of packets")
plt.title("Scheme")
plt.legend(legend)
#plt.show()
# save the figure
plt.savefig('VC8_0.4.eps', format='eps')

