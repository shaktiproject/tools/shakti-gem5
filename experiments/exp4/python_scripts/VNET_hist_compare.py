#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def plotfig(lis1, lis2, inj_rate1, inj_rate2, name):
		#plotting graph1 for vent1
		minimum = min(lis1)
		maximum = max(lis1)
		print("Total packets: " + str(len(lis1)))
		print("The max latency is : " + str(maximum) + " Cycles")
		print("The min latency is : " + str(minimum) + " Cycles")
		legend = ['injection rate = ' + inj_rate1]
		bins = maximum - minimum
		plt.subplot(221) # the first figure
		plt.hist(lis1,bins)
		plt.xlabel("Latency ")
		plt.ylabel("Number of packets")
		plt.title("baseline")
		plt.legend(legend)

		#plotting graph2 for vnet2
		minimum = min(lis2)
		maximum = max(lis2)
		print("Total packets: " + str(len(lis2)))
		print("The max latency is : " + str(maximum) + " Cycles")
		print("The min latency is : " + str(minimum) + " Cycles")
		legend = ['injection rate = ' + inj_rate2]
		bins = maximum - minimum
		plt.subplot(222) # the second figure
		plt.hist(lis2,bins)
		plt.xlabel("Latency ")
		plt.ylabel("Number of packets")
		plt.title("Scheme")
		plt.legend(legend)
		#plt.show()
		# save the figure
		plt.savefig(name, format='eps')

# get file1


inj_rate1 = 0.4


inj_rate2 = 0.4
for j in range(1,4):
		lis0 = [] # vnet0
		lis1 = [] # vent1
		for i in range(4*(j-1),4*j):
				#filename = input("Which VC? ")
				fname0 = 'lat_baseline_0.4/' + 'latency' + str(i) + '.txt'
				fname1 = 'lat_Scheme_0.4/' + 'latency' + str(i) + '.txt'
				#print(" ")
				with open(fname0) as f:
				  content0 = f.readlines()

				content0 = [x.strip() for x in content0]

				for x in content0:
				  lis0.append(int(x))

				with open(fname1) as f:
				  content1 = f.readlines()

				content1 = [x.strip() for x in content1]

				for x in content1:
				  lis1.append(int(x))
		plotfig(lis0, lis1, str(inj_rate1), str(inj_rate2), "VNET" + str(j-1) + "_0.4.eps")





#print(lis)





