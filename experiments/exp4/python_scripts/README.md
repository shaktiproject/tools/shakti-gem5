### Overview of scripts:

* avg_lat.py : prints the statistcs avg_latency, max_latency, total packets, 95% avg_lat, 95$ latency stopper for each VC (Files like baseline0.24.txt are generated using this script)

* avg_lat1.py : prints above with min_latency, total packets and average latency for the compelte network.

* hist.py : draws histogram for a VC given their latency data. (input file ex: latency0.txt)

* hist_compare.py : draws two histogram for given latency files (used to compare VC performance)

* VNET_avg_lat.py : similar to avg_lat.py but prints statistics for each VNET. (Files like VNET_baseline0.24.txt are generated using this script)

* VNET_hist_compare.py : similar to hist_compare.py, generates image which compares histogram for each VNET of two schemes. (Files like VNET0_0.24.eps are generated using this script)
