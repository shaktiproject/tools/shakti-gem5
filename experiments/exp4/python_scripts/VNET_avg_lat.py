average = 0
total_packets = 0
lis0 = [] # for vnet0
lis1 = [] # for vnet1
lis2 = [] # for vnet2

for i in range(12):
    #filename = input("Which VC? ")
    fname = 'lat_Scheme_0.4/' + 'latency' + str(i) + '.txt'
    #print(" ")
    with open(fname) as f:
      content = f.readlines()

    content = [x.strip() for x in content]
    lis = []

    for x in content:
      lis.append(int(x))

       #print(lis)

    if(int(i/4) == 0):
      #print("loop of vnet0")
      for x in lis:
        lis0.append(x)

    if(int(i/4) == 1):
      #print("loop of vnet1")
      for x in lis:
        lis1.append(x)

    if(int(i/4) == 2):
      #print("loop of vnet2")
      for x in lis:
        lis2.append(x)

    # edit by rishabh
    average += sum(lis)
    total_packets += len(lis)# end

#print(len(lis0))
#print(len(lis1))
#print(len(lis2))


#vnet0
print("=======")
print("VNET0" + "stats: ")
print("avg_lat is: " + str(sum(lis0) / float(len(lis0))))
print("min_lat is:" + str(min(lis0)))
print("max_lat is:" + str(max(lis0)))
print("total packets: " + str(len(lis0)))
print("")

#vnet1
print("=======")
print("VNET1" + "stats: ")
print("avg_lat is: " + str(sum(lis1) / float(len(lis1))))
print("min_lat is:" + str(min(lis1)))
print("max_lat is:" + str(max(lis1)))
print("total packets: " + str(len(lis1)))
print("")

#vnet2
print("=======")
print("VNET2" + "stats: ")
print("avg_lat is: " + str(sum(lis2) / float(len(lis2))))
print("min_lat is:" + str(min(lis2)))
print("max_lat is:" + str(max(lis2)))
print("total packets: " + str(len(lis2)))
print("")

# finding 95 percentile..where does the 95 percentile packets lie ?
#step1 : sort the list
#step2: sweep through the list till 0.95 * len(lis)(covering 95 % packets)
# step3: find the average latency for them

lis0.sort()
lis1.sort()
lis2.sort()

#for vnet0
total = 0
for x in range(int(0.95 * len(lis0))):
  total += lis0[x]
print("VNET0 : 95% avg_lat: " + str(total / (float(0.95 * len(lis0)))))
# value of latency at which to the left lies 95 % of packets
nintey_five_latency = lis0[int(0.95 * len(lis0))]
print("VNET0: 95% latency stopper is: " + str(nintey_five_latency))
print("======")
print("")

#for vnet1
total = 0
for x in range(int(0.95 * len(lis1))):
  total += lis1[x]
print("VNET1: 95% avg_lat: " + str(total / (float(0.95 * len(lis1)))))
nintey_five_latency = lis1[int(0.95 * len(lis1))]
print("VNET1: 95% latency stopper is: " + str(nintey_five_latency))
print("======")
print("")

#for vnet2
total = 0
for x in range(int(0.95 * len(lis2))):
  total += lis2[x]
print("VNET2: 95% avg_lat: " + str(total / (float(0.95 * len(lis2)))))
nintey_five_latency = lis2[int(0.95 * len(lis2))]
print("VNET2: 95% latency stopper is: " + str(nintey_five_latency))
print("======")
print("")

print("total latency is: " + str(average))
print("total packets are: " + str(total_packets))
print("Average latency of network is: " + str(average / (1.0 * total_packets)))
