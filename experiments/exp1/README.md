## Obtaining packet trace

### configuration for garnet:
* Mesh_XY topology
* network: garnet2.0
* cache coherence protocol: garnet standalone (dummy cache coherence protocol)
* rows in mesh: 4
* number of cpu's: 16
* number of directories: 16
* simulation cycles: 10000
* synthetic: uniform_random (model of synthetic traffic)

### Steps followed:

1 . Modifications in source file 'src/mem/ruby/network/garnet2.0/NetworkInterface.cc' :
* Added header files iostream and fstream for stdout and file manipulation.
* Added snippet to write latency of received packet in file 'latency.txt'


2 . Rebuild gem5 :
```bash
$ scons build/NULL/gem5.debug PROTOCOL=Garnet_standalone -j9
```

3 . Script hist.py:
* Goes through the file 'latency.txt' line by line and stores packet latency in a list
* Histogram is plotted over different values of injection_rates to understand behavior of NoC.
* Also checks for maximum and minimum latency

4 .To run garnet synthetic traffic for a** 4*4 mesh **topology having 16 cpus, 16 directories, uniform
random synthetic traffic, simulation cycles of 1000 and 0.01 injection rate:

```bash
$ ./build/NULL/gem5.debug configs/example/garnet_synth_traffic.py --num-cpus=16 --num-dirs=16 --         network=garnet2.0 --topology=Mesh_XY --mesh-rows=4  --sim-cycles=10000 --synthetic=uniform_random --injectionrate=0.01
```

5 . Script extract_lat.sh:
* Extracting average_packet_latency values from 'm5out/stats.txt'
* runs garnet for different injection rates
* stores the average_packet_latency in a file 'log_avg_lat.txt'
* This gives an idea of behavior of packet_latency for variation in injection_rates.

### Observation:

1 . File 'log_avg_lat.txt' stores avg_latency values for injection rates ranging from 0.02 to 0.5.
As injection rate increases, the avg_latency value gradually increases and then shoots up.
From this we can infer that for higher injection rates, the network is having a situation of high
congestion which can lead to deadlocks.

 **NOTE:** Every cycle, each CPU performs a Bernoulli trial with probability equal to the set injection rate to determine whether to generate a packet or not. The injection rate is defined as the number of packets per node per cycle.

2 . Histogram behavior:

* The subdirectory 'histogram' stores the pictures of histograms for various injection rates.
* As injection rate increases, the number of packets in the network increases
* For higher injection rates, two peaks are observed and a long tail of latency is present for small
number of packets.
* ![injection_rate = 0.05](./histogram/Figure_2.png "0.05")
* ![injection_rate = 0.5](./histogram/Figure_11.png "0.5")

3 . Max latency behavior: As injection rate increases, the max latency increases.

4 . Min latency behavior: remains fixed to 3.

### Results:

1 . [spreadsheet](https://docs.google.com/spreadsheets/d/1Z2XUhrsqm-Z7c8s-M_K0zBus8bSbDo4DK9lYonUKVjM/edit?usp=sharing): tabular description fo injection rates, received packets, max_latency
    and minimum latency. This table can be checked parallely with corresponding histogram.

