### A brief description about the experiment directories:

*  exp1 : obtaining packet trace
*  exp2 : VC priority scheme
*  exp3 : sweep analysis to obtain "latency vs throughput" curve
*  exp4 : criticality designated to VNET
*  exp5 : criticality designated to VCs for each VNET
*  exp6 : simulating NoC on a benchmark with cache coherence