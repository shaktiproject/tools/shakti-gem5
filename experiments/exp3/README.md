## Sweep Analysis

### Aim :
To determine the `saturation throughput` and `zero load latency` of any Network on Chip model.

### Some Essentials: 
* Performance of network is measured in terms of network latency or accepted traffic[1].

* Zero-load latency is the latency experienced by a packet when there are no other packets in the network. It provides a lower bound on average message latency and is found by taking the average distance (given in terms of network hops) a message will travel times the latency to traverse a single hop.

* In addition to providing ultra-low latency communication, networks must also deliver high throughput.

* A high saturation throughput indicates that the network can accept a large amount of traffic before all packets experience very high latencies, sustaining higher bandwidth.

* `Figure1` : presents a standard latency vs throughput curve for an on-chip network illustrating the zero-load latency and saturation throughput.
* Figure1 : ![Figure1](./pictures/standard_curve.jpg "Figure1 : standard latecny vs throughput curve")


### Steps followed : 
* Run garnet model 25 times for injection rates in this domain [0.02, 0.5] for step size of 0.02.

* The bash script `scripts/extract_lat.sh` does this and stores the average network packet latency in a file 'log_avg_lat.txt'.

* Following network configurations are chosen for analysis:
1.  simulation cycles = 10k , network model = baseline garnet
2.  simulation cycles = 100k, netowrk model = baseline garnet
3.  simulation cycles = 10k , network model = scheme in exp4(criticality designation to VCs)
4.  simulation cycles = 100k, network model = scheme in exp4(criticality designation to VCs) 

* The python script `scripts/four_graph.py` plots the curve for above 4 cases in one image.

### Observation and Results :
* `Figure2` : presents the graph for above 4 cases. It can be inferred that : 
    *  Saturation throughput is around 0.3 injection rate (packet/node/cycle)
    *  The network can be divided into three regions: Till Zero load latency, Between zero load latency and saturation , post saturation.
    *  Network latency in multiples of 100 or greater than 3 times average latency, generally means that the network has entered into saturation. 
* Figure2 : ![Figure2](./pictures/four_sweep.jpg "Figure2 : 4 case latecny vs throughput curve")





### References :
[1] https://doi.org/10.2200/S00772ED1V01Y201704CAC040

