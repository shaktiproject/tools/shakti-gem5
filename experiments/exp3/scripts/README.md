### Overview of scripts:

*  extract_lat.sh : Runs garnet 25 times for different injection rates

*  sweep_inj_rate_plot.py : Plots a latency vs throughput curve.

*  four_graph.py : Plots a latency vs throughput curve for four latency files in one image.
