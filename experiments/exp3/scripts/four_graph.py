import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')

def parser(fname):
	with open(fname) as f:
		content = f.readlines()

	# convert content to list
	content = [x.strip() for x in content]
	for i in range(len(content)):
		content[i] = content[i].split()
		content[i] = float(content[i][-1]	)

	return content

content1 = parser('DAS_log_10k.txt')
content2 = parser('DAS_log_100k.txt')
content3 = parser('baseline_log_10k.txt')
content4 = parser('baseline_log_100k.txt')

inj_rate = []
for i in range(1,26):
	inj_rate.append(0.02*i)

#plot these four data on one graph
plt.plot(inj_rate,content1, 'black')
plt.plot(inj_rate,content2, 'blue')
plt.plot(inj_rate,content3, 'red')
plt.plot(inj_rate,content4, 'green')
plt.xlabel("injection rate")
plt.ylabel("average latency")
plt.legend(['Scheme with 10k cycles','Scheme with 100k cycles', 'baseline with 10k cycles', 'baseline with 100k cycles' ])
plt.title("latency vs injection rate curve")

#display legend for clarity

#plt.show()
plt.savefig("four_sweep.eps", format='eps')

