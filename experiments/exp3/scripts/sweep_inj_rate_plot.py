import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.pyplot.plot
plt.style.use('seaborn-whitegrid')

inj_rate = []
for i in range(1,26):
  inj_rate.append(0.02*i)

fname = 'log_avg_lat.txt'

with open(fname) as f:
  content = f.readlines()

# convert into a list
content = [x.strip() for x in content]
# remove the first element as it is empty line
content.remove(content[0])
for i,x in enumerate(content):
  print("inj_rate : " + str(0.02 * (i+1)) + " has latency: " + str(float(x)))
  content[i] = float(x)

# now content on y axis (avg latency values), inj_rate on x_axis
plt.plot(inj_rate,content)
plt.xlabel("injection rate")
plt.ylabel("average latency")
plt.title("latency vs injection rate curve")
plt.show()
