## VC priority scheme :

### Aim of the experiment - 
To implement a scheme of prioritizing certain Virtual Channels to improve boundedness of the packet delivery.

### Plan of action
Modify Garnet to give a particular VC higher priority over other VC's for a fixed number of cycles.

### Results

1. Histogram behavior: 
* The subdirectory 'histogram' stores the pictures of histograms for two cases: no priority(Fig1) and with priority(Fig2) for VC0. 
* Fig1 : ![no_priority.png](./histogram/no_priority.png "0.5")
* Fig2 : ![has_priority.png](./histogram/has_priority.png "0.5")

2. [Spreadsheet](https://docs.google.com/spreadsheets/d/1IvLlgaglut1C6RjzxubwcEDVQOS6xd5FdPNPUo8m6hQ/edit?usp=sharing): tabular description of all VCs comparing above two cases.

### configuration for garnet:
* Mesh_XY topology
* network: garnet2.0
* cache coherence protocol: garnet standalone (dummy cache coherence protocol)
* rows in mesh: 2
* number of cpu's: 4
* number of directories: 4
* simulation cycles: 30000
* synthetic: uniform_random (model of synthetic traffic)
* injection rate: 0.5

### Steps followed:

1 . Modifications in source file 'src/mem/ruby/network/garnet2.0/NetworkInterface.cc' :
* Added header files iostream and fstream for stdout and file manipulation.
* Added snippet to write latency of received packet for each VC in different files.

2 . Modifications in source file 'src/mem/ruby/network/garnet2.0/SwitchAllocator.cc':
* in method ' arbitrate_inports()'  VC0 is given 3 cycles extra compared to other VC's.
* The number of cycles, id of VC can be changed in this file.

3 .To run garnet synthetic traffic for a **2*2 mesh** topology having 4 cpus, 4 directories, uniform random synthetic traffic, simulation cycles of 30000 and 0.05 injection rate:

```bash
$ ./build/NULL/gem5.debug configs/example/garnet_synth_traffic.py --num-cpus=4 --num-dirs=4 --network=garnet2.0 --topology=Mesh_XY --mesh-rows=2  --sim-cycles=30000 --synthetic=uniform_random --injectionrate=0.5
```

4 .  Rebuild gem5 :

```bash
$ scons build/NULL/gem5.debug PROTOCOL=Garnet_standalone -j9
```

5 . Script hist.py:
* Asks for latency input file path.
* Asks for injection rate.
* Goes through the file line by line and stores packet latency in a list
* Prints max and minimum latency on the console.

6 . Script avg_lat.py:
* Iterates through all files in 'lat' directory.
* Calculates the 95% value of latency for which 95% of packets lie left to this value.
* Prints the stats: avg_latency, max_latency, total_packets for each VC.