//this program check hardID, archID and mstatus of RISCV core
//inline assembly is written to capture csrr values
// hex values of csrr are copied from RISCV privilege spec Page No. 13


#include <stdio.h>


int main(int argc, char* argv[])
{

  int hartID ;
	int status ;
	int archid;

	/*asm ("mov %1, %0\n\t"
	    "add $1, %0"
	    : "=r" (dst)
	    : "r" (src));
	*/
	asm ("csrr %0,0xF14\n\t"
		:"=r" (hartID)
		);

	asm ("csrr %0,0xF12\n\t"
		:"=r" (archid)
		);

	asm ("csrr %0,0x300\n\t"
		:"=r" (status)
		);
	/*asm ("csrr x11,0xF14\n\t"
		"add x0, x11, x0");
*/	int *x;
	x = &hartID;

	for (int i = 0; i < 2; i++)
		printf ("%d \n",*(x+ i)  );

	printf("mHART ID is %d and mstatus is is %d and archid is %d \n", hartID, status, archid);

}
